import React from "react";
import Footer from "../Components/Footer/Footer";
import Header from "../Components/Header/Header";

function Layout({ children }) {
  return (
    <div>
      <Header />
      {children}
      <Footer />
    </div>
  );
}

export default Layout;
