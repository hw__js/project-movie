import { userLocalService } from "../../service/localService";
import { SET_USER_LOGIN, SET_USER_REGISTER } from "../constant/userConstant";

const initialState = { user: userLocalService.get() };

export const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_USER_LOGIN:
      return { ...state, user: payload };
    case SET_USER_REGISTER:
      return { ...state };
    default:
      return state;
  }
};
