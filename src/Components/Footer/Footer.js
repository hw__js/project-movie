import React from "react";
// import { Desktop, Mobile, Tablet } from "../../HOC/Responsive";
import "../../assets/css/footer.css";
import "../../assets/css/variables.css";
import "../../assets/css/index.css";
import BackToTop from "../BackToTop/BackToTop";

export default function Footer() {
  return (
    <>   
      <footer className="footer mt-20">
        <div className="new-slaters">
          <div className="container mx-auto">
            <div className="container new-slaters__container mx-auto">
              <div className="new-slaters__wrap">
                <h5 className="mb-5 cate">subscribe to Boleto </h5>
                <h3 className="mb-10 title">to get exclusive benifits</h3>
                <form className="new-slaters__form sm:w-full md:w-7 lg:w-1/2">
                  <input type="email" placeholder="Your Email Address" />
                  <button className="new-slaters__btn">Subscribe</button>
                </form>
                <p className="pt-8 text-white text-xl">
                  We respect your privacy, so we never share your info
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="footer__container container mx-auto">
          <div className="footer__top md:flex lg:flex lg:justify-between lg:items-center md:justify-between md:items-center">
            <div className="logo">
              <img
                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPYAAACUCAYAAACgLtSZAAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH5wELFQUXyb0/MgAAEnFJREFUeNrt3Xl0FGW+xvFvAgkJBMKSEMgChEVFQFFEQBAURBzUq3JGgRkd9epwRocZnVHH/eI4ZwRFkYveAUUYlKtXYBwGxAUEJGzKJkokCKisISxJWLNv94+3uru6093V3eAI7fM5p04n3fW+XdVVv3qXeqsKREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREjDbAdOAgUG6bvgTu1M8jAq2AO4A5wBbgEFBhm/KBTcAbwPVAox95eTOAvUBdkGmiNuuPIyaK1y3f9ncdUAQU2l4L/bxX4JPu36EF8ATwuzCD9SDwKDAbGAgMjeC7PwA+8/P+DNvfG61Sudoqof8C3ASkADFtEuEvveDGdtA8Hr45Bi9/DW9+607/IvCIT/5NrfSpIU4u2dZ28tUc+BkwCOgBdABaAg1t8xwHdgEbgAXAUqBGgX3uBXV6BOkqga7A92GmiwWygI7WlGX77JkgJfQjwB+AeIBWjWB4FtzQDrq2gLREEyyF5XCwDDYXwvzdsDQfKmrd+Sy3gvOJCLbnLOB1n+CeAdwGJNne2w68YK2Le90yGsNnN0FWUv2MX9wCj6xz/3sV8LItUCOtbZzyCe6LgceAm4GEMPPaD4wHXovGAI+JsmB2rVNbgPzbraK63ASH69X+d1GFeS0ogfzSkIO7B3CfFcSdgPZAXIB5/+wT3K4S+kFXidIiHp64FH7XHRo1CKGoLoVHP4fZO836AQxqC0Mzwyiq98Jnh9zBXWs7QI0GGs0YBBuPwPRtUF3nP4/3hsKIjoG/46qFkFMAwFdWEHqK7DhISYDUBEhNdH7NfsdsJ+vA0AL4O9DfXWTHw8+yYFA69GgJHZpCy0bQMBaOV8KuE7DhCCywDow1ddEd3DFRFNReJXR8LFSMCaOoroGu78L3JwGYZ5VagYL6PaBLvSI7CTo2M1NWEjyz0as9egAYAsy1qokADMmAuUOhZUL4K708H275GE5UwQXNIW8kxIS4RWd9A3evAOCktR+4y924WCi+G5LiYPsxcxBZsNs7feOGcOxuiAtyIJqeB2NWmr+7JMOcoZ5ADeUA5lXXnmkC1KpCD3Htuxe3gscugZs7QELDEIvqUzB+M7yWBzV1rAKujrbAbhgF6xDvr9rdtrEVbaFmEgsT+sFtSwC4FbgCWOtTQnex2nFxPVrCfd1NEHdKhvZJ9Xfy1/PggKkFdLPagNPsv/k9XWHaIFOqRGJwFiy/CYa9b9q2i/PhunahpR2Y4dXe9dK/DSRZleXzW8K/hsPKA/DwWthw2LyfkgBxccG/I7Op9+97SevIN3KVp+lxDUDrRHi5P4zuEvrBzL1czWBUF5i6NXo7mKIhsNv6fbNJeIENcOt5cMUWWHsQgElAP6C7bwmdFAfvDYcuzYPn162VO7DvsWoA7l1wREeYPjj8ndJXrzZmWQbPh8lb4LoOoaXr2AIymkB+Sf3Prm1X/7cbmAnrboU5O+Hxz+BwGVTVBS+xraYNAOU14W8P3xqVS782MH84pDWOLK+KGhizwtOMsTVFokZs1Ad2mNOkge7o6wPc5a/a/beroEtL57y6tXInGWkP6m4t4c1rIaZBZMvoOw3Kgkcvg8V7Ie9o6OlspbaXYR38zx/TAEZdABelQGk1LNobPP8539qCqfb01tHVzr+oFSy5GdKSIs9r/CZTw8GcRhyDV4wrsM/qwE6PcMP3SYdR57uzecU3qO+4AO7oFlpe3VL8L/C0IZCUcGaC2jWN62tKsClbwghsP51tKQlwSVrgNLnF8P4uM+8DOVap7Ge+qbmwdJ93KRnpulXVeTqE3hzm/Nt9uAeumQ/Np0Hq63Dfp3Cq2ny27SiM9/R9jAe+icaqeDQEtt/TWm2TrD0hgmn8AEgwVcwm9jw7J8PfhoSej63Edrs+GwZkRr5sgaZGcfD7nvBWHhRXhJbGX2APbQ8xsYHTPLfeU7ztOwW9/w9mboUjZVBZC18XwW+Wwf2feudbXh35ulVaX9i3LfRMCz7vR7vh+gWwbJ/pbCssh2m5cPtis9xjlpnlBLZZga029jlVFU+K/LDVvjk82AsmrPfu/Hn3Rqu0CFG31PrvPdjrhzucjrkYnloLr+fCY32c5++aAimJUFhmq4ZnB16+nUdh7g7v9wpK4J5PQmvXRrrerhL7olTnPFYd8P/+gu/gvuWw2nxeZ1XBKxXY51pgNz29AHq8L8z8Gg5bHUATBkGvtuHl0SwRMpPM6RVXp9vAdj9cYKckQY9UePVLeLiPc297DHBlJszfaSuxgwT2+PWeXqbEhqaXOyURUhubV/vfqY0h/ySMWGB1ftVCXUxknYVV1mtJtfNvd/+lsOYArNxf/7PXtrgrG/cBq4liP8nArqtz3sGaJcKzA+A3S2B4R3iwNxGd9e+W6gnsQe0gPi60dNW1sKEAispMmzejaWjpBmbBq1/AP3bAqAtDm98V2N1TIL2Z//n2noD/zbOtVwqsuSN43psO+pTade4mjtuKvfBOHpyqtKYqn9dKOGmVq6v2m5PNDYIEd2Yy5PwSthXC4l2Qsw9W7TMDkmxbcApmBNtTmKGyCuxzKrADBGLBKdhzAvplBM/43p4mQGbdYLU7fZysgKYOgyO7p5odDCA7ObSDw6d74K4PTDABNIiBydfA2F7OabOtU3CTN8KobiEEtu2897COgZfvhXVe55LxCpNgVQJ7O7sGEnwObF8dhulfhbah95yAV76wDrJOzYxUMz14OdTWwfoDMH8HzP4aCkqIx4yzvwwYHo1V8qjsFW8YA6lBesVrYuDpVTj2xjZoCB+PCpzXuDXOedjb2aGcotlaBNfP8wQ1mOGPv/8ENh1yTt/KOre7rgA+L3Cev2dbaBZv0lzb0f88h0phxlcB9p5gk09g+z3lFWYt6KFlMGWTqdaH2qse2wD6ZsHzQ2D3WJhwtdlHMCPYPsQaq6/APrtqHPW6qNKSzMYMGNjAst2mmhZKcPt7f+G3sHBnCIFtG23VpJHz/P+9Ecr8VA7rgInrnNM3s3XuTd4Q2k4/IMtUka9s73+el9ZbA0x8S+NQAsunKu77eZumpqe7SwvT4dks3tRQAqkFHvgE+syCd7eZ4bThnDqLj4NH+8PHvzAdolZwR93lped6VTzN38HJqUfctY8+nQMr7wr/S2tq4fHlUFjqfGi8MM3EQB1QXO48/3dHA3+2+aBz+uIKz9/vbTPt+8xmDtXxDmYASKKfZkVxGUzdFGGx4FsVr62fZmQPM9mt2w99ZwbPekMBjJ5vxrVfkQXXZMPgbOidHnw0nMuQTjBxKDywGICxwEuY68tVYp/1HWdBSmyAVXvhk12EPWDizS2QVwjHK6xTMUHmTUqAdsnm+46UOuedHiQIK0MYvXXINkS0ug7+Z6NzmoEd4NrO/j+bst50ZPkN2nBL7HBGnxkHnXaAqlrI2QNPr4D+f4cWL8DNc2D+dufq+tg+7m0TS5Td8SV6AzvIIAZ7rfLp5YQ1WKK8Gsat8KQvKnNO46qO5x52nvfq7MAre0WWc/qtR7zTvL4JSquCp7ksA248v/77JythyjqHEtlp8uk8C/m3tvrLrB7skId8llTBgu0wYg7c/0Hw74htAHf2dCftrcA+2wO7WWglNsC6fPgghLayvQTbb+vYKioPoZ2dZuZdn2+VfkHmvb0ndPEzYq1JHDx5VfC0tcCS7+pXpWfnBk8XFwfnta7//tSNcLTcYe853c6zWFNzGvYWXDoNMl+Egd7V8GnAz4F/AO5fPpRLNBftcF7G3p7Rd+crsM/ywE53CGzfGwf81/LQgvpoBYxf6Z22MIzArq6FJd87dO7Ew/J7YMSFkBRvpuu6wOoxcGGb4GnX7rfa/T4mrw2vF5lYKKuBSWscSuswq+LlAcaL7z1hDkibC8ygFtuVXH2APMyFOJdirmknNgYW3wXNHUYBXpLuvHznt46a/qao6jzzP0482aHzzKck+eIAzN8Gtzic9x2fA8d8SrDCMufDY3fb4eflNTCiR/D5M1vAe3eE/2M8l+P//W8KTeAMOy/0vN7Y5G6vVxDoVkZOxUKgNraf7TWkkwnoqhrTbq6y/X3wJJyqxH2vltEXw8BO8PZI+Pnb/s8itGoME65zXsaGURXO0RPYgaviQU6Z1PhpsY1bCjd3Czwibd8xeMXPbf8KS3E8F9u1tadnfPUeWLUbrsw+sz/E2j3w0Y7An09eA8NCrGxW1cBET83kX5jLTh17vZ1U1PhPM7izmQJ5+AN4aZV1rIiBpwabfIZ3hfVj4fkcWLkLDp+Cds3NAezJqyEthNF6+44rsM+dwHYqsf28l3sQ5ubCyJ7+04xbal2h5KMohFNejRMguyV8X2z+//U/4YsHoPEZGhZxvAzunBt8nsU7YNsR6JrmnN9bG907fBXmLqgjA1bFwyixI73ZQoeWnr9HXQwXtLHVhtJh9ujIf7vlnjvb7VMb+ywO7BggLYzOM7tnPrE+8x0NdhjeDHAu110Vd2pn23bG7Ufgj4s4I9dh18XAr+bAt0UmxoFV/pazDpiy1jm/GmCC55LLtzD3N4+s8yzWT4kdwTp2aGUrrYdyxq5hr6yFGZ4r+JYrsM8OMZgBKl5aJ0HDOIedN8DJk2+OwDtf1p//8Q/NeGO/gV0SfmADvPY5vLDi9HbMWuDeebAwzx27vwKeC1YSFzsciOZucR8kaoAJjntPGL3i5acZ2CN7Qte2Zy6wn10K+aafvRLve6krsH9Erf01Jdzt62DnsYOcFf3zYiip9My7ehe8nxd4/sISQrvpgp9Gw6OL4KEFprc83JsPFJXCDW/AzPXuoL4XWAgsxtxEoJ7SKpi+LnCedcD4Ze7Z5wLfBj2shnkeuyKM89jFpbB8J0zKgWeXmNL66WGcsRtTzPvKa12f5d//oAi1scNpXx8rh5dWmABPTzavbZO9x1AHu8/sd0WQ9BikNIHM5lB4yv3RLsylft6l/GGzk3RKMVNyov98uwV4fMGkHMj5Hl6+Ba7s5LzS1TUwaz08uch0FmEK7l8DM+21bmCqv/SvroKHBkNDP8MuF+ZCboE7D+e7i4Tbxg5wPfXuIvgy30yb95vXvT5Day9Kh86tz0xR9Noa+O08dy1sHlF4J5Wo6+zfXQwPL/DTgRVnBXpy4Gq1b0lc6BmeuRozQGKy73y7iuG2WZ7/U5Ogcwp0TvVMnVIgI9mUOv6+e9M+GDgFLm8Pt14CAzqaASPJiSaQD52ErQWwdDu8uwkOeAbIFGGe9/WRb60b+Cu2+5e77D8Ov5gNPTPMgSujuef1r0s8MQ7knm5gV/gcQU9WwpYCK3hdUz4cK3PeHlsOQP/JMOOX0CM9sn0j/xj84Z8wb7P7rXmYByRE3V1Kz+UHBvQENp9G+tWY63EDDXPYDPynK84x9xb/8CxZ91qrhH4SOBxgngmYa47DdQoYjHnGFdZvtMF3pl5Z8PFY2FNspr3F9f8uLPkBSqJYuPcK+NNQyE4JLc3hkzB1JUxcajWzrIIb+C1R+vyuczmwO2CuynkojDQbgT/aguMg5vEzTfwE/WjMI2BcsoHnMQ8TCEelLfiaWt8VaU2pCvNInmewRmEFkWk1H0L5rmrM42/xCeqAgf0DKcY8gvdKAj8yyVQWYqBvNgzvDv2yoX0rU/OIiYGSClO9X7cbluTBolyvm0TkY56XNg85q4P7ReBt4FPMrWSP4/+RrkuwPevJR4pVA3BNgZ6ClW11Km0CjhH8EbJ1mHOj+3wqrb2AlSGktU9VmCdehvssjVuttMHyPknwCyAuC3NZ66zOux0O8+wC5gPjgP8A7M8w6W0dEOvO4FSKeY5ak59CYETr0zYbY4abtvWpvq45w9+TCnT2mTphntXl0j5AG+5yK/AGAOcByT4l6CFgK+ZZVe+GUEIHC27XwSrD9kqAErperRv42Dr4hWKtdQB9Au+BLZut0tg1HXPIpzfmFFSP09xGJVZH4hN47ouowBaxBbfLHmva6+fvwjPZpMacxvsTfs5IODWtrYCeaAX3T4oCW84FsUBfzI0H+1m1oEyf/bcE2A2ss5pdi35KJbSIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIRLf/BykqUejk8d3IAAAAAElFTkSuQmCC"
                alt=""
              />
            </div>
            <div className="social-icons pb-5 md:pb-0 space-x-4">
              <a href="/">
                <i class="fab fa-facebook"></i>
              </a>
              <a href="/">
                <i class="fab fa-twitter"></i>
              </a>
              <a href="/">
                <i class="fab fa-pinterest-p"></i>
              </a>
              <a href="/">
                <i class="fab fa-google"></i>
              </a>
              <a href="/">
                <i class="fab fa-instagram"></i>
              </a>
            </div>
          </div>
          <div className="footer__bottom md:flex md:items-center md:justify-between text-white">
            <p>
              Copyright © 2020.All Rights Reserved By
              <img
                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPYAAACUCAYAAACgLtSZAAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH5wELFQUXyb0/MgAAEnFJREFUeNrt3Xl0FGW+xvFvAgkJBMKSEMgChEVFQFFEQBAURBzUq3JGgRkd9epwRocZnVHH/eI4ZwRFkYveAUUYlKtXYBwGxAUEJGzKJkokCKisISxJWLNv94+3uru6093V3eAI7fM5p04n3fW+XdVVv3qXeqsKREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREREjDbAdOAgUG6bvgTu1M8jAq2AO4A5wBbgEFBhm/KBTcAbwPVAox95eTOAvUBdkGmiNuuPIyaK1y3f9ncdUAQU2l4L/bxX4JPu36EF8ATwuzCD9SDwKDAbGAgMjeC7PwA+8/P+DNvfG61Sudoqof8C3ASkADFtEuEvveDGdtA8Hr45Bi9/DW9+607/IvCIT/5NrfSpIU4u2dZ28tUc+BkwCOgBdABaAg1t8xwHdgEbgAXAUqBGgX3uBXV6BOkqga7A92GmiwWygI7WlGX77JkgJfQjwB+AeIBWjWB4FtzQDrq2gLREEyyF5XCwDDYXwvzdsDQfKmrd+Sy3gvOJCLbnLOB1n+CeAdwGJNne2w68YK2Le90yGsNnN0FWUv2MX9wCj6xz/3sV8LItUCOtbZzyCe6LgceAm4GEMPPaD4wHXovGAI+JsmB2rVNbgPzbraK63ASH69X+d1GFeS0ogfzSkIO7B3CfFcSdgPZAXIB5/+wT3K4S+kFXidIiHp64FH7XHRo1CKGoLoVHP4fZO836AQxqC0Mzwyiq98Jnh9zBXWs7QI0GGs0YBBuPwPRtUF3nP4/3hsKIjoG/46qFkFMAwFdWEHqK7DhISYDUBEhNdH7NfsdsJ+vA0AL4O9DfXWTHw8+yYFA69GgJHZpCy0bQMBaOV8KuE7DhCCywDow1ddEd3DFRFNReJXR8LFSMCaOoroGu78L3JwGYZ5VagYL6PaBLvSI7CTo2M1NWEjyz0as9egAYAsy1qokADMmAuUOhZUL4K708H275GE5UwQXNIW8kxIS4RWd9A3evAOCktR+4y924WCi+G5LiYPsxcxBZsNs7feOGcOxuiAtyIJqeB2NWmr+7JMOcoZ5ADeUA5lXXnmkC1KpCD3Htuxe3gscugZs7QELDEIvqUzB+M7yWBzV1rAKujrbAbhgF6xDvr9rdtrEVbaFmEgsT+sFtSwC4FbgCWOtTQnex2nFxPVrCfd1NEHdKhvZJ9Xfy1/PggKkFdLPagNPsv/k9XWHaIFOqRGJwFiy/CYa9b9q2i/PhunahpR2Y4dXe9dK/DSRZleXzW8K/hsPKA/DwWthw2LyfkgBxccG/I7Op9+97SevIN3KVp+lxDUDrRHi5P4zuEvrBzL1czWBUF5i6NXo7mKIhsNv6fbNJeIENcOt5cMUWWHsQgElAP6C7bwmdFAfvDYcuzYPn162VO7DvsWoA7l1wREeYPjj8ndJXrzZmWQbPh8lb4LoOoaXr2AIymkB+Sf3Prm1X/7cbmAnrboU5O+Hxz+BwGVTVBS+xraYNAOU14W8P3xqVS782MH84pDWOLK+KGhizwtOMsTVFokZs1Ad2mNOkge7o6wPc5a/a/beroEtL57y6tXInGWkP6m4t4c1rIaZBZMvoOw3Kgkcvg8V7Ie9o6OlspbaXYR38zx/TAEZdABelQGk1LNobPP8539qCqfb01tHVzr+oFSy5GdKSIs9r/CZTw8GcRhyDV4wrsM/qwE6PcMP3SYdR57uzecU3qO+4AO7oFlpe3VL8L/C0IZCUcGaC2jWN62tKsClbwghsP51tKQlwSVrgNLnF8P4uM+8DOVap7Ge+qbmwdJ93KRnpulXVeTqE3hzm/Nt9uAeumQ/Np0Hq63Dfp3Cq2ny27SiM9/R9jAe+icaqeDQEtt/TWm2TrD0hgmn8AEgwVcwm9jw7J8PfhoSej63Edrs+GwZkRr5sgaZGcfD7nvBWHhRXhJbGX2APbQ8xsYHTPLfeU7ztOwW9/w9mboUjZVBZC18XwW+Wwf2feudbXh35ulVaX9i3LfRMCz7vR7vh+gWwbJ/pbCssh2m5cPtis9xjlpnlBLZZga029jlVFU+K/LDVvjk82AsmrPfu/Hn3Rqu0CFG31PrvPdjrhzucjrkYnloLr+fCY32c5++aAimJUFhmq4ZnB16+nUdh7g7v9wpK4J5PQmvXRrrerhL7olTnPFYd8P/+gu/gvuWw2nxeZ1XBKxXY51pgNz29AHq8L8z8Gg5bHUATBkGvtuHl0SwRMpPM6RVXp9vAdj9cYKckQY9UePVLeLiPc297DHBlJszfaSuxgwT2+PWeXqbEhqaXOyURUhubV/vfqY0h/ySMWGB1ftVCXUxknYVV1mtJtfNvd/+lsOYArNxf/7PXtrgrG/cBq4liP8nArqtz3sGaJcKzA+A3S2B4R3iwNxGd9e+W6gnsQe0gPi60dNW1sKEAispMmzejaWjpBmbBq1/AP3bAqAtDm98V2N1TIL2Z//n2noD/zbOtVwqsuSN43psO+pTade4mjtuKvfBOHpyqtKYqn9dKOGmVq6v2m5PNDYIEd2Yy5PwSthXC4l2Qsw9W7TMDkmxbcApmBNtTmKGyCuxzKrADBGLBKdhzAvplBM/43p4mQGbdYLU7fZysgKYOgyO7p5odDCA7ObSDw6d74K4PTDABNIiBydfA2F7OabOtU3CTN8KobiEEtu2897COgZfvhXVe55LxCpNgVQJ7O7sGEnwObF8dhulfhbah95yAV76wDrJOzYxUMz14OdTWwfoDMH8HzP4aCkqIx4yzvwwYHo1V8qjsFW8YA6lBesVrYuDpVTj2xjZoCB+PCpzXuDXOedjb2aGcotlaBNfP8wQ1mOGPv/8ENh1yTt/KOre7rgA+L3Cev2dbaBZv0lzb0f88h0phxlcB9p5gk09g+z3lFWYt6KFlMGWTqdaH2qse2wD6ZsHzQ2D3WJhwtdlHMCPYPsQaq6/APrtqHPW6qNKSzMYMGNjAst2mmhZKcPt7f+G3sHBnCIFtG23VpJHz/P+9Ecr8VA7rgInrnNM3s3XuTd4Q2k4/IMtUka9s73+el9ZbA0x8S+NQAsunKu77eZumpqe7SwvT4dks3tRQAqkFHvgE+syCd7eZ4bThnDqLj4NH+8PHvzAdolZwR93lped6VTzN38HJqUfctY8+nQMr7wr/S2tq4fHlUFjqfGi8MM3EQB1QXO48/3dHA3+2+aBz+uIKz9/vbTPt+8xmDtXxDmYASKKfZkVxGUzdFGGx4FsVr62fZmQPM9mt2w99ZwbPekMBjJ5vxrVfkQXXZMPgbOidHnw0nMuQTjBxKDywGICxwEuY68tVYp/1HWdBSmyAVXvhk12EPWDizS2QVwjHK6xTMUHmTUqAdsnm+46UOuedHiQIK0MYvXXINkS0ug7+Z6NzmoEd4NrO/j+bst50ZPkN2nBL7HBGnxkHnXaAqlrI2QNPr4D+f4cWL8DNc2D+dufq+tg+7m0TS5Td8SV6AzvIIAZ7rfLp5YQ1WKK8Gsat8KQvKnNO46qO5x52nvfq7MAre0WWc/qtR7zTvL4JSquCp7ksA248v/77JythyjqHEtlp8uk8C/m3tvrLrB7skId8llTBgu0wYg7c/0Hw74htAHf2dCftrcA+2wO7WWglNsC6fPgghLayvQTbb+vYKioPoZ2dZuZdn2+VfkHmvb0ndPEzYq1JHDx5VfC0tcCS7+pXpWfnBk8XFwfnta7//tSNcLTcYe853c6zWFNzGvYWXDoNMl+Egd7V8GnAz4F/AO5fPpRLNBftcF7G3p7Rd+crsM/ywE53CGzfGwf81/LQgvpoBYxf6Z22MIzArq6FJd87dO7Ew/J7YMSFkBRvpuu6wOoxcGGb4GnX7rfa/T4mrw2vF5lYKKuBSWscSuswq+LlAcaL7z1hDkibC8ygFtuVXH2APMyFOJdirmknNgYW3wXNHUYBXpLuvHznt46a/qao6jzzP0482aHzzKck+eIAzN8Gtzic9x2fA8d8SrDCMufDY3fb4eflNTCiR/D5M1vAe3eE/2M8l+P//W8KTeAMOy/0vN7Y5G6vVxDoVkZOxUKgNraf7TWkkwnoqhrTbq6y/X3wJJyqxH2vltEXw8BO8PZI+Pnb/s8itGoME65zXsaGURXO0RPYgaviQU6Z1PhpsY1bCjd3Czwibd8xeMXPbf8KS3E8F9u1tadnfPUeWLUbrsw+sz/E2j3w0Y7An09eA8NCrGxW1cBET83kX5jLTh17vZ1U1PhPM7izmQJ5+AN4aZV1rIiBpwabfIZ3hfVj4fkcWLkLDp+Cds3NAezJqyEthNF6+44rsM+dwHYqsf28l3sQ5ubCyJ7+04xbal2h5KMohFNejRMguyV8X2z+//U/4YsHoPEZGhZxvAzunBt8nsU7YNsR6JrmnN9bG907fBXmLqgjA1bFwyixI73ZQoeWnr9HXQwXtLHVhtJh9ujIf7vlnjvb7VMb+ywO7BggLYzOM7tnPrE+8x0NdhjeDHAu110Vd2pn23bG7Ufgj4s4I9dh18XAr+bAt0UmxoFV/pazDpiy1jm/GmCC55LLtzD3N4+s8yzWT4kdwTp2aGUrrYdyxq5hr6yFGZ4r+JYrsM8OMZgBKl5aJ0HDOIedN8DJk2+OwDtf1p//8Q/NeGO/gV0SfmADvPY5vLDi9HbMWuDeebAwzx27vwKeC1YSFzsciOZucR8kaoAJjntPGL3i5acZ2CN7Qte2Zy6wn10K+aafvRLve6krsH9Erf01Jdzt62DnsYOcFf3zYiip9My7ehe8nxd4/sISQrvpgp9Gw6OL4KEFprc83JsPFJXCDW/AzPXuoL4XWAgsxtxEoJ7SKpi+LnCedcD4Ze7Z5wLfBj2shnkeuyKM89jFpbB8J0zKgWeXmNL66WGcsRtTzPvKa12f5d//oAi1scNpXx8rh5dWmABPTzavbZO9x1AHu8/sd0WQ9BikNIHM5lB4yv3RLsylft6l/GGzk3RKMVNyov98uwV4fMGkHMj5Hl6+Ba7s5LzS1TUwaz08uch0FmEK7l8DM+21bmCqv/SvroKHBkNDP8MuF+ZCboE7D+e7i4Tbxg5wPfXuIvgy30yb95vXvT5Day9Kh86tz0xR9Noa+O08dy1sHlF4J5Wo6+zfXQwPL/DTgRVnBXpy4Gq1b0lc6BmeuRozQGKy73y7iuG2WZ7/U5Ogcwp0TvVMnVIgI9mUOv6+e9M+GDgFLm8Pt14CAzqaASPJiSaQD52ErQWwdDu8uwkOeAbIFGGe9/WRb60b+Cu2+5e77D8Ov5gNPTPMgSujuef1r0s8MQ7knm5gV/gcQU9WwpYCK3hdUz4cK3PeHlsOQP/JMOOX0CM9sn0j/xj84Z8wb7P7rXmYByRE3V1Kz+UHBvQENp9G+tWY63EDDXPYDPynK84x9xb/8CxZ91qrhH4SOBxgngmYa47DdQoYjHnGFdZvtMF3pl5Z8PFY2FNspr3F9f8uLPkBSqJYuPcK+NNQyE4JLc3hkzB1JUxcajWzrIIb+C1R+vyuczmwO2CuynkojDQbgT/aguMg5vEzTfwE/WjMI2BcsoHnMQ8TCEelLfiaWt8VaU2pCvNInmewRmEFkWk1H0L5rmrM42/xCeqAgf0DKcY8gvdKAj8yyVQWYqBvNgzvDv2yoX0rU/OIiYGSClO9X7cbluTBolyvm0TkY56XNg85q4P7ReBt4FPMrWSP4/+RrkuwPevJR4pVA3BNgZ6ClW11Km0CjhH8EbJ1mHOj+3wqrb2AlSGktU9VmCdehvssjVuttMHyPknwCyAuC3NZ66zOux0O8+wC5gPjgP8A7M8w6W0dEOvO4FSKeY5ak59CYETr0zYbY4abtvWpvq45w9+TCnT2mTphntXl0j5AG+5yK/AGAOcByT4l6CFgK+ZZVe+GUEIHC27XwSrD9kqAErperRv42Dr4hWKtdQB9Au+BLZut0tg1HXPIpzfmFFSP09xGJVZH4hN47ouowBaxBbfLHmva6+fvwjPZpMacxvsTfs5IODWtrYCeaAX3T4oCW84FsUBfzI0H+1m1oEyf/bcE2A2ss5pdi35KJbSIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIRLf/BykqUejk8d3IAAAAAElFTkSuQmCC"
                alt=""
              />
            </p>
            <ul className="md:space-x-5 lg:space-x-7 pb-3">
              <li>
                <a href="">About</a>
              </li>
              <li>
                <a href="">Terms Of Use</a>
              </li>
              <li>
                <a href="">Privacy Policy</a>
              </li>
              <li>
                <a href="">FAQ</a>
              </li>
              <li>
                <a href="">Feedback</a>
              </li>
            </ul>
          </div>
        </div>

      </footer>
      <BackToTop/>
    </>
  );
}
