import autoprefixer from "autoprefixer";
import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import "../../assets/css/backToTop.css"

export default function BackToTop() {
  const [backToTopButton, setBackToTopButton] = useState(false);

  useEffect(() => {
    window.addEventListener("scroll", () => {
      if (window.scrollY > 250) {
        setBackToTopButton(true);
      } else {
        setBackToTopButton(false);
      }
    });
  }, []);

  const scrollUp = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  return (
    <div>
      {backToTopButton && (
        <button className="text-white backToTop" onClick={scrollUp}>
          <i class="fa fa-angle-up"></i>
        </button>
      )}
    </div>
  );
}
