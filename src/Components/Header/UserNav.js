import React from "react";
import { useSelector } from "react-redux";
import { userLocalService } from "../../service/localService";
import { Button, Dropdown } from "antd";
import "../../assets/css/userNav.module.css";

function UserNav() {
  let user = useSelector((state) => {
    return state.userReducer.user;
  });

  const handleLogout = () => {
    // delete data from localStore
    userLocalService.remove();
    // logout user to login page
    // window.location.href = "/login";

    window.location.reload();
  };
  const items = [
    {
      key: "1",
      label: (
        <div className="px-4 py-3">
          <span className="block text-center text-sm text-gray-900 dark:text-white">
            {user?.hoTen}
          </span>
          <span className="block text-sm font-medium text-gray-500 truncate dark:text-gray-400">
            {user?.email}
          </span>
        </div>
      ),
    },
    {
      key: "2",
      label: (
        <a
          className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-200 dark:hover:text-white"
          target="_blank"
          rel="noopener noreferrer"
        >
          Profile
        </a>
      ),
    },
    {
      key: "3",
      label: (
        <button
          className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-200 dark:hover:text-white"
          onClick={handleLogout}
        >
          Logout
        </button>
      ),
    },
  ];

  const renderContent = () => {
    if (user) {
      // user logined
      return (
        <>
          <Dropdown
            menu={{
              items,
            }}
            placement="bottom"
            arrow
          >
            <Button className="border-none pr-12">
              <img
                className="w-10 h-10 rounded-full"
                src="https://i.pinimg.com/originals/66/41/ad/6641ad0f9d53d5ea9a22002e0326e587.jpg"
                alt="user photo"
              />
            </Button>
          </Dropdown>
        </>
      );
    } else {
      return (
        <>
          <button
            onClick={() => {
              window.location.href = "/login";
            }}
            className="border-2 border-slate-400 px-5 py-2 rounded hover:bg-blue-700 hover:text-white"
          >
            Đăng nhập
          </button>
          <button
            onClick={() => {
              window.location.href = "/register";
            }}
            className="border-2 border-slate-400  px-5 py-2 rounded hover:bg-blue-700 hover:text-white"
          >
            Đăng kí
          </button>
        </>
      );
    }
  };
  return <div className="space-x-3">{renderContent()}</div>;
}

export default UserNav;
