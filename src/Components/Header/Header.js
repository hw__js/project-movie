import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import UserNav from "./UserNav";
import logo from "../../assets/logo.png";

import styles from "./Header.module.scss";

const Button = () => {
  return <button className={styles.button}>Click me</button>;
};

function Header() {
  const [menuOpen, setMenuOpen] = useState(false);
  const menuToggler = () => setMenuOpen((p) => !p);

  return (
    // px-20 py-5 flex justify-between items-center
    // <div className="">
    //   {/* <NavLink to="/">
    //     <span className="text-red-600 text-3xl font-medium">CyberFlix</span>
    //   </NavLink> */}

    <div className={styles.header}>
      <div className={styles.header__content}>
        <div>
          <NavLink to="/">
            <img className={styles.logo} src={logo} alt="" />
          </NavLink>
        </div>

        <div>
          <nav
            className={`${styles.nav} ${menuOpen ? styles[`nav--open`] : {}}`}
          >
            <a className={styles.nav__item} href={"/"}>
              Home
            </a>
            <a className={styles.nav__item} href={"/"}>
              Pages
            </a>
            <a className={styles.nav__item} href={"/"}>
              Service
            </a>
            <a className={styles.nav__item} href={"/"}>
              Contact
            </a>
            <div className={styles.nav__button__container}>
              <UserNav />
            </div>
          </nav>
        </div>

        <div>
          <div className={styles.header__button__container}>
            <UserNav />
          </div>
          <button className={styles.header__toggler} onClick={menuToggler}>
            {!menuOpen ? (
              <svg
                className="w-6 h-6"
                aria-hidden="true"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                  clipRule="evenodd"
                />
              </svg>
            ) : (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke-width="1.5"
                stroke="currentColor"
                class="w-6 h-6"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  d="M6 18L18 6M6 6l12 12"
                />
              </svg>
            )}
          </button>
        </div>
      </div>
    </div>
  );
}

export default Header;
