import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getDetailMovie } from "../../service/movieService";
import "../../assets/css/DetailPage.css";
// import Footer from './../../Components/Footer/Footer';
// import { Button } from 'antd';

function DetailPage() {
  let { id } = useParams();
  const [detailMovie, setDetailMovie] = useState(null);

  useEffect(() => {
    getDetailMovie(id)
      .then((res) => {
        console.log("res: ", res);
        setDetailMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [id]);

  return (
    <>
      {detailMovie && (
        <>
          <div
            className="detailMovie__banner"
            style={{
              backgroundImage: `url(${detailMovie.hinhAnh})`,
            }}
          >
            <div className="detailMovie-content">
              <div className="container mx-auto">
                <div className="detailMovie-content__container flex">
                  <div className="detailMovie-content__poster w-full flex">
                    <div
                      className="detailMovie-content__poster__img"
                      style={{ backgroundImage: `url(${detailMovie.hinhAnh})` }}
                    ></div>
                    <div className="detailMovie-content__info">
                      <h1 className="title text-3xl mb-2 font-bold text-white">
                        {detailMovie.tenPhim}
                      </h1>
                      <div className="language">
                        <span>ENGLISH</span>
                        <span>HINDI</span>
                        <span>TELEGU</span>
                        <span>TAIMIL</span>
                      </div>
                      <button>HONOR</button>
                      <p className="text-white text-base mb-4">
                        {detailMovie.moTa.length < 60
                          ? detailMovie.moTa
                          : detailMovie.moTa.slice(0, 60) + "..."}
                        {/* {detailMovie.ngayKhoiChieu} */}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="book-section">
                <div className="container mx-auto flex">
                  <div className="book-section__left opset-1 w-full flex">
                    <div className="book-section__list flex items-center">
                      <div className="book-section__item mr-5">
                        <div className="item__header mb-3 flex items-center">
                          <img
                            className="mr-2"
                            src="https://pixner.net/boleto/demo/assets/images/movie/tomato2.png"
                            alt=""
                          />
                          <span className="font-bold text-xl">88%</span>
                        </div>
                        <p className="">Tomatometer</p>
                      </div>
                      <div className="book-section__item mr-5 opset-23 ">
                        <div className="item__header mb-3 flex items-center">
                          <img
                            className="mr-2"
                            src="https://pixner.net/boleto/demo/assets/images/movie/cake2.png"
                            alt=""
                          />
                          <span className="font-bold text-xl">88%</span>
                        </div>
                        <p>Audience Score</p>
                      </div>
                      <div className="book-section__item mr-5 ">
                        <div className="item__header mb-2 mt-1 flex items-center">
                          <span className="text-2xl font-bold mr-4">
                            {detailMovie.danhGia}
                          </span>
                          <div className="rated space-x-1">
                            <i className="fas fa-heart"></i>
                            <i className="fas fa-heart"></i>
                            <i className="fas fa-heart"></i>
                            <i className="fas fa-heart"></i>
                            <i className="fas fa-heart"></i>
                          </div>
                        </div>
                        <p>Users Rating</p>
                      </div>
                      <div className="book-section__item ml-2 opset-12">
                        <div className="item__header mb-2 mt-1 flex items-center">
                          <div className="rated rated-it space-x-1">
                            <i className="fas fa-heart"></i>
                            <i className="fas fa-heart"></i>
                            <i className="fas fa-heart"></i>
                            <i className="fas fa-heart"></i>
                            <i className="fas fa-heart"></i>
                          </div>
                          <span className="text-2xl font-bold ml-4">0.0</span>
                        </div>
                        <p>Rate It</p>
                      </div>
                    </div>

                    <button className="btn__detail opset-4 ">
                      book tickets
                    </button>
                  </div>
                  <div className="book-section__right"></div>
                </div>
              </div>
            </div>

            <div className="detailMovie__bottom pt-28 opset-2">
              <div className="container mx-auto">
                <div className="detailMovie__wrap md:flex">
                  <div className="detailMovie__left">
                    <div className="title text-white text-2xl mb-8">
                      Applicable offer
                    </div>
                    <div className="offer">
                      <img
                        src="https://pixner.net/boleto/demo/assets/images/sidebar/offer01.png"
                        alt=""
                      />
                      <h3>Amazon Pay Cashback Offer</h3>
                      <p>Win Cashback Upto Rs 300*</p>
                    </div>
                    <div className="offer">
                      <img
                        src="https://pixner.net/boleto/demo/assets/images/sidebar/offer03.png"
                        alt=""
                      />
                      <h3>PayPal Offer</h3>
                      <p>
                        Transact first time with Paypal and get 100% cashback up
                        to Rs. 500
                      </p>
                    </div>
                    <div className="offer">
                      <img
                        src="https://pixner.net/boleto/demo/assets/images/sidebar/offer02.png"
                        alt=""
                      />
                      <h3>HDFC Bank Offer</h3>
                      <p>
                        Get 15% discount up to INR 100* and INR 50* off on F&B
                        T&C apply
                      </p>
                    </div>
                  </div>
                  <div className="detailMovie__right">
                    <div className="header">
                      <h2 className="title">PHOTOS</h2>
                      <div className="md:flex">
                        <div className="item">
                          <img
                            src="https://pixner.net/boleto/demo/assets/images/movie/movie-details01.jpg"
                            alt=""
                          />
                        </div>
                        <div className="item">
                          <img
                            src="https://pixner.net/boleto/demo/assets/images/movie/movie-details02.jpg"
                            alt=""
                          />
                        </div>
                        <div className="item">
                          <img
                            src="https://pixner.net/boleto/demo/assets/images/movie/movie-details03.jpg"
                            alt=""
                          />
                        </div>
                      </div>
                    </div>
                    <div className="body opset-2">
                      <h2 className="sub-title">Synopsis</h2>
                      <p>
                        Lorem ipsum, dolor sit amet consectetur adipisicing
                        elit. Architecto consequatur quia fuga eveniet sit natus
                        delectus quam. Earum magnam id ratione vero dolore
                        consectetur tempora maxime atque! Culpa sint magnam
                        incidunt numquam sequi corrupti perferendis debitis
                        dicta quae consequatur, vero, sed nemo sunt distinctio
                        soluta vitae quod magni cupiditate quis eius nostrum.
                        Ratione iure quas eveniet optio et delectus rerum?
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
}

export default DetailPage;
