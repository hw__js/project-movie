import { Card } from "antd";
import React from "react";
// import { NavLink } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import "../../../assets/css/movieList.css";
import { Pagination } from "antd";
const { Meta } = Card;

function MovieList({ movieArr }) {
  let navigate = useNavigate();

  const handleNavigate = (id) => {
    setTimeout(() => {
      navigate(`/detail/${id}`);
    }, 1000);
  };

  const renderMovieList = () => {
    return movieArr.slice(0, 8).map((item, index) => {
      return (
        <Card
          key={index}
          className="mb-6 bg-movie-list"
          hoverable
          cover={
            <div className="card-top">
              <img
                className="h-80 w-full object-cover"
                alt="example"
                src={item.hinhAnh}
              />
              <div className="overlay">
                <button
                  className="btn-detail btn mb-5"
                  onClick={() => {
                    handleNavigate(item.maPhim);
                  }}
                >
                  View detail
                </button>
                <button className="btn-trailer btn">View Trailer</button>
              </div>
            </div>
          }
        >
          <Meta
            title={
              <div>
                <h2 className="text-blue-500">{item.tenPhim}</h2>
                <ul className="flex space-x-4 pt-5">
                  <li className="flex items-center">
                    <img
                      src="https://pixner.net/boleto/demo/assets/images/movie/tomato.png"
                      alt=""
                    />
                    <span className="content">88%</span>
                  </li>
                  <li className="flex items-center">
                    <img
                      src="https://pixner.net/boleto/demo/assets/images/movie/cake.png"
                      alt=""
                    />
                    <span className="content">88%</span>
                  </li>
                </ul>
              </div>
            }
          />
        </Card>
      );
    });
  };
  return (
    <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-7">
      {renderMovieList()}
      {/* <Pagination defaultCurrent={1} total={50} /> */}
    </div>
  );
}

export default MovieList;
