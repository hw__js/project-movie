import React, { useEffect, useState } from "react";
import { getMovieByTheater } from "../../../service/movieService";
import { Tabs } from "antd";
import MovieItemTab from "./MovieItemTab";
import "../../../assets/css/movieTab.css"
const onChange = (key) => {
  console.log(key);
};

export default function MovieTab() {
  const [dataMovie, setDataMovie] = useState([]);
  useEffect(() => {
    getMovieByTheater()
      .then((res) => {
        setDataMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const renderDanhSachPhimTheoCumRap = (cumRap) => {
    return cumRap.danhSachPhim.map((movie, index) => {
      return <MovieItemTab movie={movie} key={index} />;
    });
  };

  const renderCumRapTheoHeThongRap = (heThongRap) => {
    return heThongRap.lstCumRap.map((cumRap) => {
      return {
        label: (
          <div className="w-44">
            <h5 className="text-white">{cumRap.tenCumRap}</h5>
            <p className="text-white truncate">{cumRap.diaChi}</p>
          </div>
        ),
        key: cumRap.maCumRap,
        children: (
          <div className="scroll-y" style={{ height: 500 }}>
            {renderDanhSachPhimTheoCumRap(cumRap)}
          </div>
        ),
      };
    });
  };

  const renderHeThongRap = () => {
    return dataMovie.map((heThongRap) => {
      return {
        label: <img className="w-16 h-16" src={heThongRap.logo} alt="" />,
        key: heThongRap.maHeThongRap,
        children: (
          <Tabs
            style={{ height: 500 }}
            tabPosition="left"
            defaultActiveKey="1"
            onChange={onChange}
            items={renderCumRapTheoHeThongRap(heThongRap)}
          ></Tabs>
        ),
      };
    });
  };

  return (
    <div className="container movie-tab mx-auto mt-20">
      <Tabs
        style={{ height: 500 }}
        tabPosition="left"
        defaultActiveKey="1"
        onChange={onChange}
        items={renderHeThongRap()}
      />
    </div>
  );
}
