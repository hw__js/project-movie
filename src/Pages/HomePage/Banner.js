import React from "react";
import { Carousel } from "3d-react-carousal";
import "../../assets/css/banner.css";
import banner1 from "../../assets/banner1.png";
import banner2 from "../../assets/banner2.png";
import banner3 from "../../assets/banner3.png";
import banner4 from "../../assets/banner4.png";
import banner5 from "../../assets/banner5.png";

function Banner() {
  let slides = [
    <img src={banner1} alt="1" />,
    <img src={banner2} alt="2" />,
    <img src={banner3} alt="3" />,
    <img src={banner4} alt="4" />,
    <img src={banner5} alt="5" />,
  ];
  return (
    <div className="banner">
      <Carousel
        className="banner__content"
        slides={slides}
        autoplay={true}
        interval={3000}
      />
    </div>
  );
}

export default Banner;
