import React, { useEffect, useState } from "react";
import { getMovieList } from "../../service/movieService";
import MovieList from "./MovieList/MovieList";
import MovieTab from "./MovieTab/MovieTab";
import "../../assets/css/homePage.css";
import Banner from "./Banner";

function HomePage() {
  const [movieArr, setMovieArr] = useState([]);
  useEffect(() => {
    getMovieList()
      .then((res) => {
        console.log(res);
        setMovieArr(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div style={{ background: "#001232" }}>
      <Banner />
      <div className="movie-list container mx-auto">
        <h1 className="title text-white text-center text-4xl pt-16 pb-16">
          UPCOMINGS MOVIES
        </h1>
        <MovieList movieArr={movieArr} />
        <MovieTab />
      </div>
    </div>
  );
}

export default HomePage;
