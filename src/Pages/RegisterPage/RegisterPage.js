import React from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import {
  AutoComplete,
  Button,
  Cascader,
  Checkbox,
  Col,
  Form,
  Input,
  InputNumber,
  Row,
  Select,
} from "antd";
import { setUserRegisterService } from "../../redux/action/userAction";
import "../../assets/css/RegisterPage.css";
import FormItem from "antd/es/form/FormItem";
import Lottie from "lottie-react";
import logo_register from "../../assets/lf30_editor_9sll5of3.json";

const { Option } = Select;

function RegisterPage() {
  let navigate_R = useNavigate();
  let dispatch_R = useDispatch();

  const onFinish = (values) => {
    console.log("Success:", values);
    const handleNavigate = () => {
      setTimeout(() => {
        navigate_R("/login");
      }, 1000);
    };
    dispatch_R(setUserRegisterService(values, handleNavigate));
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  // const prefixSelector = (
  //   <Form.Item name="prefix" noStyle>
  //     <Select
  //       style={{
  //         width: 70,
  //       }}
  //     >
  //       <Option value="86">+86</Option>
  //       <Option value="87">+87</Option>
  //     </Select>
  //   </Form.Item>
  // );

  return (
    <div>
      <h2 className="registerTitle pt-5 pb-10">Register</h2>
      <div className="container register__content">
        <div className="container__register ">
          <Form
            className="register__form"
            // layout="inline"
            name="basic"
            labelCol={{
              span: 6,
            }}
            wrapperCol={{
              span: 22,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <div
              className="flex gap-5"
              style={{ marginBottom: "-20px", marginTop: "-20px" }}
            >
              <FormItem className="w-3/5">
                <Form.Item
                  label="Username"
                  name="taiKhoan"
                  rules={[
                    {
                      required: true,
                      message: "Please input your username!",
                    },
                  ]}
                >
                  <Input />
                </Form.Item>

                <Form.Item
                  label="Password"
                  name="matKhau"
                  rules={[
                    {
                      required: true,
                      message: "Please input your password!",
                    },
                  ]}
                >
                  <Input.Password />
                </Form.Item>

                <Form.Item
                  name="email"
                  label="E-mail"
                  rules={[
                    {
                      type: "email",
                      message: "The input is not valid E-mail!",
                    },
                    {
                      required: true,
                      message: "Please input your E-mail!",
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
              </FormItem>

              <FormItem className="w-3/5">
                <Form.Item
                  name="soDt"
                  label="Phone Number"
                  rules={[
                    {
                      required: true,
                      message: "Please input your phone number!",
                    },
                  ]}
                >
                  <Input
                    style={{
                      width: "100%",
                    }}
                  />
                </Form.Item>

                <Form.Item
                  name="maNhom"
                  label="Group ID"
                  hasFeedback
                  rules={[
                    {
                      required: true,
                      message: "Please select your group id!",
                    },
                  ]}
                >
                  <Select placeholder="Please select a id for your group">
                    <Option value="00">00</Option>
                    <Option value="01">01</Option>
                  </Select>
                </Form.Item>

                <Form.Item
                  name="hoTen"
                  label="Full Name"
                  tooltip="What do you want others to call you?"
                  rules={[
                    {
                      required: true,
                      message: "Please input your fullname!",
                      whitespace: true,
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
              </FormItem>
            </div>

            <Form.Item
              className="flex"
              wrapperCol={{
                offset: 2,
                span: 18,
              }}
            >
              <Button
                htmlType="submit"
                id="btn__register"
                style={{ position: "relative", top: "3%" }}
              >
                Register
              </Button>
              <div style={{ width: "40%", position: "absolute", top: "-20%" }}>
                <Lottie
                  animationData={logo_register}
                  loop={true}
                  style={{ width: "100%" }}
                />
              </div>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
}

export default RegisterPage;
