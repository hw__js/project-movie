import React from "react";
import { Button, Form, Input, message } from "antd";
import { postLogin } from "../../service/userService";
import { useNavigate } from "react-router-dom";
import { SET_USER_LOGIN } from "../../redux/constant/userConstant";
import { useDispatch } from "react-redux";
import { userLocalService } from "../../service/localService";
import Lottie from "lottie-react";
import bg_animate from "../../assets/88398-christmas-animations.json";
import { setUserActionService } from "../../redux/action/userAction";
import "../../assets/css/LoginPage.css";

function LoginPage() {
  let navigate = useNavigate();
  let dispatch = useDispatch();

  const onFinishReduxThunk = (values) => {
    const handleNavigate = () => {
      setTimeout(() => {
        navigate("/");
      }, 1000);
    };
    dispatch(setUserActionService(values, handleNavigate));
  };

  const onFinish = (values) => {
    console.log("Success:", values);
    postLogin(values)
      .then((res) => {
        console.log(res);
        //
        message.success("Dang Nhap Thanh Cong");
        //
        dispatch({ type: SET_USER_LOGIN, payload: res.data.content });

        // userAction
        // dispatch(setUserAction(res.data.content));

        // let account information to save in localStorage
        userLocalService.set(res.data.content);

        setTimeout(() => {
          // window.location => load lai trang => redux se mat
          //   window.location.href = "/";
          navigate("/");
        }, 1000);
      })
      .catch((err) => {
        console.log(err);
        message.error("Dang Nhap That Bai");
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div id="loginPage">
      <h2 className="loginTitle">Login</h2>
      <div id="loginForm">
        <div className="container container__login">
          {/* p-5 */}
          <div
            className="login__left w-2/5"
            style={{ width: "30%", translate: "-50px" }}
          >
            <Lottie
              animationData={bg_animate}
              loop={true}
              style={{ width: "100%" }}
            />
          </div>
          <div className="login__right w-3/5">
            {/* form  */}
            <Form
              layout="vertical"
              name="basic"
              labelCol={{
                span: 8,
              }}
              wrapperCol={{
                span: 24,
              }}
              initialValues={{
                remember: true,
              }}
              onFinish={onFinishReduxThunk}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                label="Username"
                name="taiKhoan"
                rules={[
                  {
                    required: true,
                    message: "Please input your username!",
                  },
                ]}
              >
                <Input placeholder="Enter your username" />
              </Form.Item>

              <Form.Item
                label="Password"
                name="matKhau"
                rules={[
                  {
                    required: true,
                    message: "Please input your password!",
                  },
                ]}
              >
                <Input.Password placeholder="Enter your password" />
              </Form.Item>

              <Form.Item
                wrapperCol={{
                  span: 24,
                }}
                className="text-center"
              >
                <Button
                  className="login__btn mb-5 border-none w-1/2"
                  htmlType="submit"
                >
                  Submit
                </Button>
                <br />
                Have not an account{" "}
                <a
                  href="/register"
                  className="p-1 text-sm hover:text-white font-medium italic"
                >
                  register now!
                </a>
              </Form.Item>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default LoginPage;
