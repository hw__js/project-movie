import React from "react";

function NotFoundPage() {
  return (
    <div className="h-screen w-screen flex justify-center items-center">
      <h1 className="text-center text-5xl font-black text-white animate-bounce">
        404 Page
      </h1>
    </div>
  );
}

export default NotFoundPage;
